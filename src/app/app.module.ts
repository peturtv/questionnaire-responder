import { BrowserModule } from '@angular/platform-browser';
import {NgModule, QueryList} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CodeableConceptComponent } from './type/codeable-concept/codeable-concept.component';
import { BooleanComponent } from './type/boolean/boolean.component';
import {FormsModule} from '@angular/forms';
import { CodeComponent } from './type/code/code.component';
import { CodingComponent } from './type/coding/coding.component';
import { IdentifierComponent } from './type/identifier/identifier.component';
import { DateTimeComponent } from './type/date-time/date-time.component';
import { IntegerComponent } from './type/integer/integer.component';
import { StringComponent } from './type/string/string.component';
import { ItemComponent } from './item/item.component';
import { ItemAnswerComponent } from './item-answer/item-answer.component';
import { QuestionnaireResponseComponent } from './questionnaire-response/questionnaire-response.component';
import { OutputComponent } from './debug/output/output.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatGridListModule,
  MatInputModule,
  MatProgressBarModule
} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import {IntegerDirective} from './type/integer/integer.directive';

@NgModule({
  declarations: [
    AppComponent,
    CodeableConceptComponent,
    BooleanComponent,
    CodeComponent,
    CodingComponent,
    IdentifierComponent,
    DateTimeComponent,
    IntegerComponent,
    StringComponent,
    ItemComponent,
    ItemAnswerComponent,
    QuestionnaireResponseComponent,
    OutputComponent,
    IntegerDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatGridListModule,
    MatProgressBarModule
  ],
  providers: [
    QueryList
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
