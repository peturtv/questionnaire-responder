import {Component, Input, OnInit} from '@angular/core';
import {FhirComponent} from '../type/FhirComponent';

@Component({
  selector: 'app-questionnaire-response',
  templateUrl: './questionnaire-response.component.html',
  styleUrls: ['./questionnaire-response.component.css']
})
export class QuestionnaireResponseComponent implements OnInit, FhirComponent {

  private qr: fhir.r4.QuestionnaireResponse;
  private answers = new Map<string, fhir.r4.QuestionnaireResponseItem>();

  private subject: string;

  @Input()
  private loading = false;

  // tslint:disable-next-line:variable-name
  private _questionnaire: fhir.r4.Questionnaire = {
    status: 'draft',
    item: []
  };

  get questionnaire(): fhir.r4.Questionnaire {
    return this._questionnaire;
  }

  @Input()
  set questionnaire(questionnaire: fhir.r4.Questionnaire) {

    // TODO: Handle merge of existing answers into the hashmap
    this.answers.clear();
    this._questionnaire = questionnaire;

    if (this._questionnaire) {
      this.qr = {
        subject: {
          reference: ''
        },
        author: {},
        status: 'draft',
        questionnaire: this._questionnaire.url // TODO: Handle containment if no url?
      };

      this.flattenItems(this._questionnaire.item).forEach(q => {
        this.answers.set(q.linkId + '', this.createResponseItemFromItem(q));
      });
    }
  }

  flattenItems(item: fhir.r4.QuestionnaireItem[]): fhir.r4.QuestionnaireItem[] {
    const result: fhir.r4.QuestionnaireItem[] = [];
    if (!item) {
      return result;
    }
    item.forEach(i => {
      result.push(i);
      result.push(...this.flattenItems(i.item));
    });
    return result;
  }

  // TODO: Does not support answer.item yet since the model here does not support answer.
  createResponses(item: fhir.r4.QuestionnaireItem[]): fhir.r4.QuestionnaireResponseItem[] {
    const result: fhir.r4.QuestionnaireResponseItem[] = [];
    if (!item) {
      return result;
    }
    item.forEach(i => {
      const response = this.createResponseItemFromItem(i);
      const children = this.createResponses(i.item);
      if (children) {
        response.item = children;
      }
      if (this.hasContent(response)) {
        result.push(response);
      }
    });
    return result.length > 0 ? result : null;
  }

  // TODO: Does not support answer.item yet since the model here does not support answer.
  createResponseItemFromItem(item: fhir.r4.QuestionnaireItem): fhir.r4.QuestionnaireResponseItem {
    return this.answers.get(item.linkId + '') ? this.answers.get(item.linkId + '') : {
      linkId: item.linkId,
      text: item.text
    };
  }

  // Checks for any content in response tree.
  // Will exit early if true.
  // Checks both .item and .answer.item
  hasContent(item: fhir.r4.QuestionnaireResponseItem): boolean {
      if (!item) {
        return false;
      }

      if (!item.answer && !item.item) {
        return false;
      }
      if (item.answer && item.answer.length > 0) {
        if (item.answer.find(a => this.hasValue(a))) {
          return true;
        }
        if (item.answer.map(a => this.listHasContent(a.item)).find(b => b === true)) {
          return true;
        }
      }

      return this.listHasContent(item.item);
  }

  private hasValue(answer) {
    return answer.valueString
      || (answer.valueBoolean !== undefined && answer.valueBoolean !== null)
      || answer.valueAttachment
      || answer.valueCoding
      || answer.valueDate
      || answer.valueDateTime
      || answer.valueTime
      || answer.valueDecimal
      || answer.valueInteger
      || answer.valueQuantity
      || answer.valueReference
      || answer.valueUri;
  }

  listHasContent(item: fhir.r4.QuestionnaireResponseItem[]): boolean {
    if (!item) {
      return false;
    }
    return item.map(i => this.hasContent(i)).find(b => b === true);
  }

  constructor() { }

  ngOnInit() {
  }

  getIdPart(reference) {
    if (reference.startsWith('http')) {
      return reference;
    } else {
      let id = reference;
      const idSplit = reference.lastIndexOf('/');
      if (idSplit > -1) {
        id = reference.substring(idSplit + 1);
        if (id.indexOf('#') > 0) {
          id = reference.substring(0, id.indexOf('#'));
        }
      }
      return id;
    }
  }

  getContents() {
    const items = this.createResponses(this.questionnaire.item);
    console.log(items);
    return items ? {...this.qr, ...{item: items}} : this.qr;
  }

  patientChanged() {
    this.qr.subject.reference = this.composeReference(this.subject, 'Patient');
  }

  composeReference(value, type) {
    return value.startsWith('http') ? value : type + '/' + value;
  }

  updateAnswer(newValue: fhir.r4.QuestionnaireResponseItem) {
    this.answers.set(newValue.linkId + '', newValue);
  }
}
