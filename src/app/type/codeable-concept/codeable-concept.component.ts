import { Component, OnInit } from '@angular/core';
import {FhirComponent} from '../FhirComponent';

@Component({
  selector: 'app-codeable-concept',
  templateUrl: './codeable-concept.component.html',
  styleUrls: ['./codeable-concept.component.css']
})
export class CodeableConceptComponent implements OnInit, FhirComponent {

  constructor() { }

  ngOnInit() {
  }

  getContents() {

  }
}
