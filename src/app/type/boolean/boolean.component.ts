import {Component, OnInit} from '@angular/core';
import {AbstractTypeComponent} from '../abstract-type/abstract-type.component';

@Component({
  selector: 'fhir-boolean',
  templateUrl: './boolean.component.html'
})
export class BooleanComponent extends AbstractTypeComponent<boolean> implements OnInit {

  private value;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.valueUpdate(false); // Enforces false to be included in response if box is not touched
  }

  createAnswer = (newValue: boolean): fhir.r4.QuestionnaireResponseItemAnswer[] => [{ valueBoolean: newValue }];
}
