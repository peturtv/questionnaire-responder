import {Component} from '@angular/core';
import {AbstractTypeComponent} from '../abstract-type/abstract-type.component';

@Component({
  selector: 'fhir-string',
  templateUrl: './string.component.html'
})
export class StringComponent extends AbstractTypeComponent<string> {

  constructor() {
    super();
  }

  createAnswer(newValue: string): fhir.r4.QuestionnaireResponseItemAnswer[] {
    return [{
      valueString: newValue
    }];
  }
}
