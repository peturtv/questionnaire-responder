
export enum QuestionnaireItemTypeEnum {
  Group = 'group',
  Display = 'display',
  Boolean = 'boolean',
  Decimal = 'decimal',
  Integer = 'integer',
  Date = 'dateTime',
  String = 'string',
  Text = 'text',
  Url = 'url',
  Choice = 'choice',
  OpenChoice = 'open-choice'
}
