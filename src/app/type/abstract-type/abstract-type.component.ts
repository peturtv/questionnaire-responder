import {EventEmitter, Input, Output} from '@angular/core';
import {FhirComponent} from '../FhirComponent';

export abstract class AbstractTypeComponent<T> implements FhirComponent {
  @Input() protected item: fhir.r4.QuestionnaireItem;
  @Input() protected answer: fhir.r4.QuestionnaireResponseItem;

  @Output() protected answerChange: EventEmitter<fhir.r4.QuestionnaireResponseItem> = new EventEmitter<fhir.r4.QuestionnaireResponseItem>();

  getContents = () => this.answer;

  // Single value answers only at the moment. Multiple answers need some refactoring separating the item and answer models better
  valueUpdate(newValue) {
    this.answer.answer = this.createAnswer(newValue);
    this.answerChange.emit(this.answer);
  }

  abstract createAnswer(newValue: T): fhir.r4.QuestionnaireResponseItemAnswer[];
}
