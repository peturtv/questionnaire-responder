import { Component } from '@angular/core';
import {AbstractTypeComponent} from '../abstract-type/abstract-type.component';

@Component({
  selector: 'fhir-integer',
  templateUrl: './integer.component.html',
  styleUrls: ['./integer.component.css']
})
export class IntegerComponent extends AbstractTypeComponent<number> {

  constructor() {
    super();
  }

  createAnswer = (newValue: number): fhir.r4.QuestionnaireResponseItemAnswer[] => [{ valueInteger: newValue}];
}
