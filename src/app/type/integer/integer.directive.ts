import { Directive, HostListener, Input } from '@angular/core';

@Directive({ selector: '[fhirInteger]' })
export class IntegerDirective {

  constructor() {}

  @Input() fhirInteger: boolean;
  private allowedValues = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
                           'home', 'end', 'arrowleft', 'arrowright', 'pageup', 'pagedown', 'tab'];
  private allowedWithModifier = ['a', 'c', 'v', 'x'];

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    if (this.fhirInteger) {
      if (     (this.isModifier(event) && this.allowedWithModifier.indexOf(event.key.toLowerCase()) >= 0)
           ||   this.allowedValues.indexOf(event.key.toLowerCase()) >= 0) {
        return;
      }
      event.preventDefault();
    }
  }

  private isModifier = (event: KeyboardEvent): boolean => event.ctrlKey || event.metaKey;

}
