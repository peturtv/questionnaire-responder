import {Component, EventEmitter, Input, Output} from '@angular/core';
import {QuestionnaireItemTypeEnum} from '../type/types';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent {

  @Input() private item: fhir.r4.QuestionnaireItem;
  @Input() private answers: Map<string, fhir.r4.QuestionnaireResponseItem>;
  @Output() answerChange: EventEmitter<fhir.r4.QuestionnaireResponseItem> = new EventEmitter<fhir.r4.QuestionnaireResponseItem>();

  questionnaireItemTypeEnum = QuestionnaireItemTypeEnum;

  constructor() { }

  isType = (text: string, type: QuestionnaireItemTypeEnum) => text === type;
}
