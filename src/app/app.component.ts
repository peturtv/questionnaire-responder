import {Component, QueryList, ViewChildren, AfterViewInit} from '@angular/core';
import {FhirComponent} from './type/FhirComponent';
import {QuestionnaireResponseComponent} from './questionnaire-response/questionnaire-response.component';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

  @ViewChildren(QuestionnaireResponseComponent) components: QueryList<FhirComponent>;

  private payload = '';
  private children: FhirComponent[];
  private httpClient: HttpClient;
  private loading = false;
  private questionnaire: fhir.r4.Questionnaire;

  private qUri = 'assets/test.json';
  private authToken = 'c328c5cb86cd47434e504d7842d642946eca7602';

  constructor(private http: HttpClient) {
    this.httpClient = http;
    this.loadForm(this.qUri);
  }

  ngAfterViewInit() {
    this.children = this.components.toArray();
  }

  loadForm(uri) {
    this.loading = true;
    this.httpClient.get<fhir.r4.Questionnaire>(uri, {
      headers: {
        Authorization: 'Bearer ' + this.authToken
      }
    }).subscribe(questionnaire => {
      this.loading = false;
      this.questionnaire = questionnaire;
    });
  }

  showData() {
    this.payload = JSON.stringify(this.children.map(c => c.getContents()), undefined, 4);
  }
}
